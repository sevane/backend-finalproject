<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');

Route::get('posts', 'Api\PostController@index');
Route::post('store', 'Api\PostController@store');
Route::post('create', 'Api\PostController@create');
Route::delete('delete/{id}', 'Api\PostController@destroy');
Route::get('edit/{id}', 'Api\PostController@edit');
Route::get('show/{id}', 'Api\PostController@show');
Route::put('update/{id}', 'Api\PostController@update');
